(() => {
    const lazyLoader = () => {}

    // Function to check if the image is available in the viewport
    const isImageInViewport = (img) => {
        const rect = img.getBoundingClientRect();

        return (
            rect.top >= 0 &&
            rect.left >= 0 &&
            rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
            rect.right <= (window.innerWidth || document.documentElement.clientWidth)
        );
    }

    // Custom Fade in effect for lazy load
    const fadeIn = (element) => {
        let elementOpacity = 0.1; // initial opacity value

        element.style.display = 'block';

        const displayTimer = setInterval(() => {
            if(elementOpacity >= 1) {
                clearInterval(displayTimer);
            }

            element.style.opacity = elementOpacity;

            element.style.filter = 'alpha(opacity=' + elementOpacity * 100 + ")";

            elementOpacity += elementOpacity * 0.1;
        
        }, 15);
    };

    // Activate main lazyLoader function
    const activateLazyLoader = () => {
        const lazyLoaderImageList = document.querySelectorAll('img[data-src]');

        lazyLoaderImageList.forEach((image) => {
            if(isImageInViewport(image)) {
                if(image.getAttribute('data-src') !== null) { // if the data-src is not empty
                    image.setAttribute('src', image.getAttribute('data-src')); // create the main image src from the data-src attribute
                    image.removeAttribute('data-src'); // remove the data-src attribute
                }

                if(image.getAttribute('data-srcset') !== null) { // if the data-srcset is not empty
                    image.setAttribute('srcset', image.getAttribute('data-srcset')); // create the main image src from the data-srcset attribute
                    image.removeAttribute('data-srcset'); // remove the data-srcset attribute
                }

                image.setAttribute('data-loaded', true); // Set a new attribute to confirm data attributes have been removed and changed to src attributes

                fadeIn(image); // Call the fadeIn function on each image
            }
        });


        // Remove all event listeners when all images have loaded in the dom
        if (document.querySelectorAll('img[data-src]').length === 0 && document.querySelectorAll('img[data-srcset]')) {

            window.removeEventListener('DOMContentLoaded', lazyLoader);

            window.removeEventListener('load', activateLazyLoader);

            window.removeEventListener('resize', activateLazyLoader);

            window.removeEventListener('scroll', activateLazyLoader);
        }
    };


    // Add event listeners to all img tags
    window.addEventListener('DOMContentLoaded', activateLazyLoader);

    window.addEventListener('load', activateLazyLoader);

    window.addEventListener('resize', activateLazyLoader);

    window.addEventListener('scroll', activateLazyLoader);


    // Check if the browser is using JS
    if(document.querySelector('.no-js') !== null) {
        document.querySelector('.no-js').classList.remove('no-js');
    }


    // Initiate lazyLoader
    return lazyLoader();

}) ();





